# Al'Packa

Al(pine)'Packa(ger) is a small shell script to automate building Alpine packages for various Alpine releases.

```shell
$ alpacka/alpacka -a ~/.abuild -d "My Repo" -r 3.18 -p aports/mypackage -o ~/packages/3.18
```
