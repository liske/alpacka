#!/bin/sh -ex

apk add --update-cache \
    alpine-conf \
    alpine-sdk \
    sudo \
  && apk upgrade -a

adduser -D builder -u "${ALPACKA_UID}" \
  && addgroup builder abuild \
  && echo 'builder ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

cp -v /mnt/abuild/*.pub /etc/apk/keys
cp -vr /mnt/abuild /home/builder/.abuild
cp -vr /mnt/source /home/builder/aport
chown -R builder:abuild /home/builder
chown -R builder:abuild /mnt/output/builder

cd /home/builder/aport

su builder -c 'export REPODEST=/mnt/output; abuild -D "${ALPACKA_DESCRIPTION}" -r -c'
